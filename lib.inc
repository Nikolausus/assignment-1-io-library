section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60                
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.counter:
    cmp byte [rdi + rax], 0
    je  .end
    inc rax
    jmp .counter
.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi			
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    pop rsi
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:                  ; Может возникнуть резонный вопрос, а чё это я функции местами в коде меняю? А это нужно чтобы не писать повторно уже написанный код
    mov rax, rdi
    test rax, rax	    ; Проверка на отрицательность
    jns .norm        
    neg rax
    mov rdi, 0x2D       
    push rax
    call print_char
    pop rax
.norm:
    mov rdi, rax            ; Типо переходим в следующую функцию


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    push 0;                 ; Крутой финт ушами, используем стек, как строку (чтобы инвертировать вывод полученных цифр)
.delenie:  
    xor rdx, rdx            ; Обнуляем rdx, тк эта паскуда (assembler) при 64bit делителе делит не rax, а rdx:rax...
    mov rdi, 0xA            ; Записываем 0xA в регистр, так как мне надо, чтобы делитель был 8-ми байтовым 
    idiv rdi
    add rdx, 0x30           ; Получаем ASCII код символа
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    jnz  .delenie
    mov rdi, rsp
    push r8
    call print_string
    pop rsp
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rdx, rdx
.cicle_srav:
    mov al, byte[rdi + rdx]
    cmp al, byte[rsi + rdx]
    jne .ne_ravni 
    test al, al
    je .ravni 
    inc rdx 
    jmp .cicle_srav
.ravni:
    mov rax, 1
    ret
.ne_ravni:
    xor rax, rax
    ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0     
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1             ; Отлавливаем ошибку
    jne .ok
    xor rax, rax
    jmp .ext
.ok:
    mov rax, [rsp]  
.ext:
    add rsp, 8
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:                  ; rdi (строка1), rsi (размер буфера)
    push r12
    push r13
    push r14
    xor r12, r12
    mov r13, rdi
    mov r14, rsi
.read_cicle:
    cmp r12, r14
    jge .err
    call read_char
    cmp rax, 0x20
    je .mid
    cmp rax, 0x9
    je .mid
    cmp rax, 0xA
    je .mid
    mov [r13 + r12], rax
    test rax, rax
    je .suc
    inc r12
    jmp .read_cicle
.err:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    ret
.mid:
    test r12, r12           ; Если начало строки и пробельные символы, то скипаем
    jz .read_cicle
.suc:   
    mov rax, r13
    mov rdx, r12
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r9, r9
    mov cl, byte [rdi]
    cmp cl, '-'
    jne .next_step
    mov r9, 0x17A
.next_step:                     
    cmp cl, '+'			; Проверка на плюс в старте у числа
    jne .perehod
    inc rdi
.perehod:			; идём дальше


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:                     ; Да видел я эти ваши тесты, 1234567890987654321hehehey - Никаких проблем с чтением и длина не равна 0
    xor rax, rax
    xor rdx, rdx
    push r9
    cmp r9, 0x17A
    jne _A
    inc rdx                     ; Типо пропускаем минус в старте строки
_A:
    mov cl, byte [rdi + rdx]
    cmp cl, '0'
    jb _END
    cmp cl, '9'
    ja _END
    and rcx, 0xF
    imul rax, 10
    add rax, rcx
    inc rdx
    jmp _A
_END:
    pop r9
    cmp r9, 0x17A               ; Если у нас в r9 записан особый код, то тогда мы инвертируем число. Да, моя программа поддерживает parse_uint с последующем изменением знака числа (если в r9 записать правильный код), что, несомненно, не потенциальный баг, а плюс
    jne _RET
    neg rax			; Негативируем число и инкриментируем его длину
_RET:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:                    ; rdi (строка1), rsi (строка2), rdx (длинна строки2)
    xor r8, r8                   ; Храним размер буфера (rdx) и номер текущего элемент (r8), если вышли за рамки, то ален капут
.copy_cicle:
    cmp r8, rdx
    je .err                     ; Достигли максимума буфера
    mov r9b, byte [rdi + r8]
    mov [rsi + r8], r9b
    inc r8
    test r9, r9                   ; По поводу того, что передаётся нуль-терминированная строка можно было и предупредить!
    je .end
    jmp .copy_cicle
.err:
    xor rax, rax
.end:
    ret
