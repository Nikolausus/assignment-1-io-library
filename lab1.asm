section .data
nel: db 10
codes:	db 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
stroka0: db '-1', 0
stroka: db '1024', 0
stroka2: db '-1024', 0
stroka3: db '112515', 0
stroka4: db '123456789', 0
stroka5: db ''

section .text
global _start 

_start:
    mov rdi, 0x200
    call print_uint
    call print_newline

    mov rdi, 0xFFFFFFFFFFFFFF00
    call print_int
    call print_newline
    
    ;call read_char
    ;mov rdi, rax
    ;call print_char
    ;call print_newline
    
    mov rdi, stroka
    mov rsi, stroka
    call string_equals
    mov rdi, rax
    call print_uint 
    call print_newline

    mov rdi, stroka
    mov rsi, stroka2
    call string_equals
    mov rdi, rax
    call print_uint 
    call print_newline

    mov rdi, stroka
    mov rsi, stroka3
    call string_equals
    mov rdi, rax
    call print_uint 
    call print_newline

    mov rdi, stroka2
    call parse_int
    mov rdi, rax
    call print_int
    call print_newline

    mov rdi, stroka3
    call parse_int
    mov rdi, rax
    call print_int
    call print_newline

    mov rdi, stroka2
    call parse_uint
    mov rdi, rax
    call print_int
    call print_newline

    mov rdi, stroka
    call parse_uint
    mov rdi, rax
    call print_int
    call print_newline

    ;mov rdi, stroka4
    ;mov rsi, stroka5
    ;mov rdx, 0x3
    ;call string_copy
    ;mov rdi, stroka5
    ;push rax
    ;call print_string
    ;call print_newline
    ;pop rdi
    ;call print_int
    ;call print_newline
    
    mov rdi, stroka5
    mov rsi, 104
    call read_word
    push rdx
    mov rdi, stroka5
    call print_string
    call print_newline 
    pop rdi
    call print_int
    call print_newline

    call exit

; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60                
    xor  rdi, rdi
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
.counter:
    cmp byte [rdi], 0
    je  .end
    inc rdi
    jmp .counter
.end:
    sub rdi, rax
    mov rax, rdi
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi			; Что за приколы с push rsi/pop rsi? Почему оно без них не работает???
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    pop rsi
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    lea rsi, [codes + rdi]
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, nel
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:                  ; Может возникнуть резонный вопрос, а чё это я функции местами в коде меняю? А это нужно чтобы не писать повторно уже написанный код
    mov rax, rdi
    mov rdi, 0x8000000000000000
    test rdi, rax	    ; Проверка на отрицательность
    jz .norm        
    neg rax
    mov rdi, 0x2D       
    push rax
    call print_char
    pop rax
.norm:
    mov rdi, rax            ; Типо переходим в следующую функцию


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    push 0;                 ; Крутой финт ушами, используем стек, как строку (чтобы инвертировать вывод полученных цифр)
.delenie:  
    xor rdx, rdx            ; Обнуляем rdx, тк эта паскуда (assembler) при 64bit делителе делит не rax, а rdx:rax...
    mov rdi, 0xA            ; Записываем 0xA в регистр, так как мне надо, чтобы делитель был 8-ми байтовым 
    idiv rdi
    add rdx, 0x30           ; Получаем ASCII код символа
    dec rsp
    mov byte[rsp], dl
    cmp rax, 0
    jnz  .delenie
    mov rdi, rsp
    push r8
    call print_string
    pop rsp
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rdx, 0
.cicle_srav:
    mov al, byte[rdi + rdx]
    cmp al, byte[rsi + rdx]
    jne .ne_ravni 
    cmp al, 0
    je .ravni 
    inc rdx 
    jmp .cicle_srav
.ravni:
    mov rax, 1
    ret
.ne_ravni:
    mov rax, 0
    ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0     
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1             ; Отлавливаем конец потока
    jne .ok
    mov rax, 0
    jmp .ext
.ok:
    mov rax, [rsp]  
.ext:
    add rsp, 8
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:                  ; rdi (строка1), rsi (размер буфера)
    mov rcx, 0
.read_cicle:
    cmp rcx, rsi
    jge .err
    push rcx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rcx
    cmp rax, 0x20
    je .mid
    cmp rax, 0x9
    je .mid
    cmp rax, 0xA
    je .mid
    mov [rdi + rcx], rax
    cmp rax, 0
    je .suc
    inc rcx
    jmp .read_cicle
.err:
    xor rax, rax
    ret
.mid:
    test rcx, rcx           ; Если начало строки и пробельные символы, то скипаем
    jz .read_cicle
.suc:   
    mov rax, rdi
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r9, 0
    mov cl, byte [rdi]
    cmp cl, '-'
    jne .next_step
    mov r9, 0x17A
.next_step:                     ; Идём дальше


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:                     ; Да видел я эти ваши тесты, 1234567890987654321hehehey - Никаких проблем с чтением и длина не равна 0
    xor rax, rax
    xor rdx, rdx
    push r9
    cmp r9, 0x17A
    jne _A
    inc rdx                     ; Типо пропускаем минус в старте строки
_A:
    mov cl, byte [rdi + rdx]
    cmp cl, '0'
    jb _END
    cmp cl, '9'
    ja _END
    and rcx, 0xF
    imul rax, 10
    add rax, rcx
    inc rdx
    jmp _A
_END:
    pop r9
    cmp r9, 0x17A               ; Если у нас в r9 записан особый код, то тогда мы инвертируем число. Да, моя программа поддерживает parse_uint с последующем изменением знака числа (если в r9 записать правильный код), что, несомненно, не потенциальный баг, а плюс
    jne _RET
    neg rax			; Негативируем число и инкриментируем его длину
_RET:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:                    ; rdi (строка1), rsi (строка2), rdx (длинна строки2)
    mov r8, 0                   ; Храним размер буфера (rdx) и номер текущего элемент (r8), если вышли за рамки, то ален капут
.copy_cicle:
    cmp r8, rdx
    je .err                     ; Достигли максимума буфера
    mov r9b, byte [rdi + r8]
    mov [rsi + r8], r9b
    inc r8
    cmp r9, 0                   ; По поводу того, что передаётся нуль-терминированная строка можно было и предупредить!
    je .end
    jmp .copy_cicle
.err:
    mov rax, 0
.end:
    ret
